$(document).ready(function(){
	
    var miLista = $("#miLista");
    var busqueda = $("#busqueda");

    $("#btn-buscar").on("click", function(){
        const palabra = $('#busqueda').val();
        console.log('Palabra a buscar: ' + palabra);
        // Realiza la búsqueda usando el API de The Movie Database
        buscarPeliculas(palabra);
    });

    function buscarPeliculas(palabra) {
        // Llama al API de The Movie Database
        $.ajax({
            url: 'https://api.themoviedb.org/3/search/movie',
            type: 'GET',
            data: {
                api_key: '8efd8c516f6784b593b8da22bcbc448b', // Reemplaza con la API key de The Movie Database
                query: palabra
            },
            success: function(response) {
                // Limpiar la lista antes de agregar nuevos resultados
                miLista.empty();

                // Iterar sobre los resultados y mostrar información relevante (títulos e imágenes)
                for (let i = 0; i < response.results.length; i++) {
                    const pelicula = response.results[i];
                    const imagenURL = 'https://image.tmdb.org/t/p/w200' + pelicula.poster_path;

                    // Agregar la película a la lista con su imagen y título
                    miLista.append('<li class="media mb-2">' +
                        '<img src="' + imagenURL + '" class="mr-3" alt="Imagen de la película">' +
                        '<div class="media-body">' +
                            '<h5 class="mt-0 mb-1">' + pelicula.title + '</h5>' +
                            '<p>' + pelicula.overview + '</p>' + // breve descripcion
                        '</div>' +
                    '</li>');
                }
            },
            error: function(error) {
                console.error('Error en la búsqueda:', error);
            }
        });
    }
});
