Arenas Reyes Carlos Ulises

Modifique un poco el codigo, esto con ayuda de unos ejemplos que encontre en yt y
el internet en general.

HTML

En el html en un div agregue la lista desordenada que
desplegaria las peliculas relacionadas con su respectiva
imagen, el titulo y una breve descripcion.

JS 

Aqui es un poco mas complicado ya que vi que tenia que generar una API KEY del sitio
Esto lo hice registrandome al sitio y pidiendo una API KEY como developer, esto fue
un poco engorroso, solo tuve que rellenar unos formularios y listo.

Hablando enteramente del codigo hice un apartado en el que js ayudaba a construir la imagen
que el sitio le daba a traves de su link proporcionado por el mismo sitio, ademas de modificar la estructura de la lista para que se pudiera incluir la imagen, descripcion etc.
Hice que la lista se limpiara cada vez que se hiciera una busqueda debido a que si no se guardaban
los resultados de busquedas anteriores y no se alcanzaba a ver el cambio a la nueva busqueda.

Cabe recalcar que los resultados de la busqueda iteran sobre un ciclo for en donde se muestran las
busquedas mas importantes.

En el js esta comentado todos los cambios que realice.
Me falto decir que borre el alert que estaba, ya que no me parecia lo mas optimo.